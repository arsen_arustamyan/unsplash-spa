import React from 'react';
import { Link } from 'react-router';

import { authenticationUrl }  from '../unsplash';

export default class Home extends React.Component {
    
    static path = '/';

    handleClick() {
        // Отправляем пользователя по этому адресу
        location.assign(authenticationUrl);
    }
    
    render() {
        return (
            <div className="container" style={{marginTop: '5%'}}>
                <div className="row">
                    <div className="col-12">
                        <div className="jumbotron">
                            <h1 className="display-3">Добро пожаловать</h1>
                            <p className="lead">Beautiful, free photos. Gifted by the world’s most generous community of photographers.</p>
                            <Link to={`/auth`}>Перейти</Link>
                            <p className="lead">
                                <button type="button" className="btn btn-primary btn-lg btn-block" onClick={this.handleClick.bind(this)}>
                                    Авторизация
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    
}
