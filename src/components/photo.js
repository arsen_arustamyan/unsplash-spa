import React from 'react';
import { Link } from 'react-router'

const Photo = (props) => {

    const { photo, index } = props;

    return (
        <div className="card" style={{width: '23rem'}}>
            <img className="card-img-top img_small" src={photo.urls.small} alt="Card image cap" />
            <div className="card-block">
                <p className="card-text">Автор: {photo.user.name}, <a href={photo.user.links.html}>{photo.user.links.html}</a></p>
                <p className="card-text">Likes: {photo.likes}</p>
                <Link to={`/photo/${index}`}>Перейти</Link>
            </div>
        </div> 
    );
};

export default Photo;