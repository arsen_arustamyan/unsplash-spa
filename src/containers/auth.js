import React, { Component } from 'react';
import { setAccessTokenUnplash, listPhoto } from '../unsplash';
import { loadPhoto } from '../action';
import { connect } from 'react-redux';

import Photo from '../components/photo'

class Auth extends Component {
    constructor(props) {
        super(props);

        if (localStorage.getItem('token') === 'undefined' || localStorage.getItem('token') === '' || !localStorage.getItem('token'))
            this.setAccessToken();
    }

    setAccessToken() {
        const code = location.search.split('code=')[1];

        if (code) {
            setAccessTokenUnplash(code);
        }
    }

    loadPhotos() {
        let start = window.localStorage.getItem('start');
        let end = window.localStorage.getItem('end');
        const data = listPhoto(+start, +end, localStorage.getItem('token'));
        data.then(d => this.props.loadPhoto(d));
        window.localStorage.setItem('start', +start + 10);
        window.localStorage.setItem('end', +end + 10);
    }

    render() {
        return (
            <div className="container" style={{marginTop: '5%'}}>
                <div className="row">
                    {
                        this.props.photos.map((photo, index) => {
                            return <Photo key={index} photo={photo} index = {index} />
                        })
                    }
                </div>
                <div className="row" style={{marginTop: '10px'}}>
                    <button type="button" className="btn btn-success btn-lg btn-block" onClick={this.loadPhotos.bind(this)}>Загрузить изображения</button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        photos: state.photoReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loadPhoto: (photos) => dispatch(loadPhoto(photos))
    }
} 

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Auth);
