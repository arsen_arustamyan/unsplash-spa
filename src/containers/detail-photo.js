import React, { Component } from 'react';
// import { getImage } from '../action';
import { connect } from 'react-redux';
import { likePhoto, unLikePhoto } from "../unsplash/index"
import { like, unlike } from "../action";

class DetailPhoto extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id_photo: ''
        }
    }

    componentDidMount() {
        const photos = this.props.photos;
        const id = this.props.params.id;
        const id_photo = photos[+id].id;

        this.setState(
            {
                id_photo
            }
        )
    }

    goBack() {
        this.props.history.goBack()
    }

    changeLikePhotoStatus() {
        const id = this.props.params.id;
        let status = this.props.photos[+id].liked_by_user;


        if (!status) {
            likePhoto(this.state.id_photo, localStorage.getItem('token'));
            this.props.likePhotoAction(this.state.id_photo);
        } else {
            unLikePhoto(this.state.id_photo, localStorage.getItem('token'));
            this.props.unlikePhotoAction(this.state.id_photo);
        }
    }

    render() {
        const id = this.props.params.id;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="card">
                        <img className="card-img-top" src={this.props.photos[+id].urls.full} alt="Card image cap" style={{maxWidth: '100%'}}/>
                        <div className="card-block">
                            <p className="card-text">Автор: {this.props.photos[+id].user.name}, <a href={this.props.photos[+id].user.links.html}>{this.props.photos[+id].user.links.html}</a></p>
                            <p className="card-text">Likes: {this.props.photos[+id].likes}</p>
                            <button type="button" onClick={this.changeLikePhotoStatus.bind(this)} className="btn btn-success">
                                { this.props.photos[+id].liked_by_user ? 'Unlike' : 'Like'}
                            </button>
                        </div>
                    </div>
                </div>
                <div className="row" style={{marginTop: '10px'}}>
                    <button type="button" className="btn btn-danger btn-lg btn-block" onClick={this.goBack.bind(this)}>Вернуться назад</button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        photos: state.photoReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        likePhotoAction: (id) => dispatch(like(id)),
        unlikePhotoAction: (id) => dispatch(unlike(id))
    }
} 

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailPhoto);
