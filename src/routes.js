import React from 'react';
import { Route } from 'react-router';
import Home from './components/home';
import Auth from './containers/auth';
import DetailPhoto from './containers/detail-photo'


export default (
    <div>
        <Route component={ Home } path={ Home.path } />
        <Route component={ Auth } path={ 'auth' } />
        <Route component={ DetailPhoto } path={ '/photo/:id' } />
    </div>
);
