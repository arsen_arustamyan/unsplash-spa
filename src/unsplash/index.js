// Подключаем библиотеку unsplash-js
// (при настроенной webpack-сборке)
import Unsplash, { toJson } from 'unsplash-js';

// Создаем экземпляр объекта для доступа к API
export const unsplash = new Unsplash({
    // Application ID из настроек вашего приложения
    applicationId: "75c270b0e3c4a57bea2913240723ef1d2132764c205245d4492ba50cf3bf4bd0",
    // Application Secret из настроек вашего приложения
    secret: "7e9aee98525f8eccd43c65bbd7b85c3ea220039539b9796487248c7f66dc84bb",
    // Полный адрес страницы авторизации приложения (Redirect URI)
    // Важно: этот адрес обязательно должен быть указан в настройках приложения на сайте Unsplash API/Developers
    callbackUrl: "http://localhost:3000/auth"
});

export const authenticationUrl = unsplash.auth.getAuthenticationUrl([
    "public",
    "write_likes"
]);

export const setAccessTokenUnplash = (code) => {
    unsplash.auth.userAuthentication(code)
        .then(res => res.json())
        .then(json =>
            localStorage.setItem('token', json.access_token)
        );
};

export const listPhoto = (start, end, access_token) => {

    unsplash.auth.setBearerToken(access_token);

    return unsplash.photos.listPhotos(start, end, "latest")
        .then(res => res.json());
};

export const likePhoto = (id, token) => {
    unsplash.auth.setBearerToken(token);

    unsplash.photos.likePhoto(id)
        .then(toJson)
        .then(json => {
            console.log(json);
        });
};

export const unLikePhoto = (id, token) => {
    unsplash.auth.setBearerToken(token);

    unsplash.photos.unlikePhoto(id)
        .then(toJson)
        .then(json => {
            console.log(json);
        });
};
